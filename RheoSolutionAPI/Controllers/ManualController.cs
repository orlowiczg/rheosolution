﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RheoSolutionAPI.ViewModels;
using RheoLib;
using RheoSolutionAPI.Validations;
using RheoSolutionAPI.Services;
using RheoSolutionAPI.Globals;

namespace RheoSolutionAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManualController : ControllerBase
    {
        // GET api/manual
        [HttpGet]
        public IActionResult ExampleData()
        {
            MeasurementInputModel manual = new MeasurementInputModel();
            manual.Angle = new double[12]{23, 24, 26, 28, 31, 35, 38, 45, 51, 65, 76, 98 };
            manual.Rate =  new int[12] {1,2,3,6,10,20,30,60,100,200,300,600};
            manual.Rotor = RotorsEnum.R1;
            manual.Bob = BobsEnum.B1;
            manual.Spring = SpringsEnum.F1;

            return Ok(manual);

        }



    }
}
