using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RheoSolutionAPI.ViewModels;
using RheoLib;
using RheoSolutionAPI.Validations;
using RheoSolutionAPI.Services;

namespace RheoSolutionAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RheomodelController : ControllerBase
    {
        // POST api/rheology
        [HttpPost]
        public IActionResult Post([FromBody] MeasurementInputModel value)
        {
            bool isValid  = value.isValid();
            if(!isValid)
            {
                ModelState.AddModelError("Error","Provide correct parameters, to see example use /api/manual");
                return BadRequest(ModelState);
            }

            return Ok(RheoLibService.GetModels(value));
        }

        [HttpPost("{id}")]
        public IActionResult Post(int id, [FromBody] MeasurementInputModel value)
        {
            bool isValid  = value.isValid();
            if(!isValid)
            {
                ModelState.AddModelError("Error","Provide correct parameters, to see example use /api/manual");
                return BadRequest(ModelState);
            }

            RheoModelViewModel output = RheoLibService.GetModel(value,id);
            if (output == null)
            {
                return NotFound(new { Error = $"Not found model with id:{id}"});
            }

            return Ok(output);
        }
    }
}
