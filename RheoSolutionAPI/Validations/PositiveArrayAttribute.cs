using System.Collections;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace RheoSolutionAPI.Validations
{
    public class PositiveArrayAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value == null)
            return  new ValidationResult($"{validationContext.DisplayName} requires initialized elements");
            var list = value as IEnumerable;

            var result = true;
            foreach(dynamic el in list)
            {
                if(el<=0)
                {
                    result = false;
                    break;
                }
            }


            return result
                ? ValidationResult.Success
                : new ValidationResult($"{validationContext.DisplayName} requires only positive elements");
        }

    }
}