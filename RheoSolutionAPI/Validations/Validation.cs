using System.Collections.Generic;
using RheoSolutionAPI.ViewModels;
using RheoSolutionAPI.Globals;

namespace RheoSolutionAPI.Validations
{
    public static class Validation
    {
        public static bool isValid(this MeasurementInputModel dto)
        {
            bool valid = false;

            if(FannParameters.CyllinderRadius.ContainsKey(dto.Rotor.ToString()) && FannParameters.CyllinderRadius.ContainsKey(dto.Bob.ToString()) && FannParameters.SpringType.ContainsKey(dto.Spring.ToString()))
                valid = true;

            if(dto.Angle.Length != dto.Rate.Length || dto.Angle.Length == 0)
                valid = false;

            return valid;
        }
    }
}