using System;
using System.ComponentModel.DataAnnotations;
using RheoSolutionAPI.Validations;
using RheoSolutionAPI.Globals;

namespace RheoSolutionAPI.ViewModels
{
    public class MeasurementInputModel
    {

        [Required]
        [PositiveArray]
        public double[] Angle {get; set;}

        [Required]
        [PositiveArray]
        public int[] Rate {get; set;}

        [Required]
        public BobsEnum Bob {get; set;}


        [Required]
        public RotorsEnum Rotor {get; set;}

        [Required]
        public SpringsEnum? Spring {get; set;}
    }
}