using System.Collections.Generic;
using RheoLib.RheoModels;
using RheoSolutionAPI.Globals;
using RheoSolutionAPI.ViewModels;

namespace RheoSolutionAPI.Services

{
    public static class RheoLibService
    {
        public static RheologyViewModel GetRheology(MeasurementInputModel input)
        {
            Measurement mes = new Measurement(
                input.Angle,
                input.Rate,
                FannParameters.CyllinderRadius[input.Rotor.ToString()],
                FannParameters.CyllinderRadius[input.Bob.ToString()],
                FannParameters.SpringType[input.Spring.ToString()],
                FannParameters.ShearStress[input.Bob.ToString()]
                );

            RheologyViewModel output = new RheologyViewModel();
            output.RotSpeed = mes.RotSpeed;
            output.Angle = mes.Angle;
            output.ApparentViscosity = mes.ApparentViscosity.ToArray();
            output.ShearRate = mes.ShearRate.ToArray();
            output.ShearStress = mes.ShearStress.ToArray();

            return output;
        }

        public static List<RheoModelViewModel> GetModels (MeasurementInputModel input)
        {
            Measurement mes = new Measurement(
                input.Angle,
                input.Rate,
                FannParameters.CyllinderRadius[input.Rotor.ToString()],
                FannParameters.CyllinderRadius[input.Bob.ToString()],
                FannParameters.SpringType[input.Spring.ToString()],
                FannParameters.ShearStress[input.Bob.ToString()]
                );

            List<IRheoModel> models = new List<IRheoModel>()
            {
                new NewtonModel(mes),
                new BinghamModel(mes),
                new CassonModel(mes),
                new OstwaldDeWaeleModel(mes),
            };

            List<RheoModelViewModel> output = new List<RheoModelViewModel>();

            foreach(var model in models)
            {
                output.Add(new RheoModelViewModel(){
                    Id = output.Count + 1,
                    Name = model.Name,
                    R = model.R,
                    F = model.F,
                    U = model.U,
                    ShearStress = model.ModeledStress.ToArray(),
                    SpecificParameters = model.SpecificParams
                });
            }
            return output;
        }

        public static RheoModelViewModel GetModel(MeasurementInputModel input, int id)
        {
            Measurement mes = new Measurement(
                input.Angle,
                input.Rate,
                FannParameters.CyllinderRadius[input.Rotor.ToString()],
                FannParameters.CyllinderRadius[input.Bob.ToString()],
                FannParameters.SpringType[input.Spring.ToString()],
                FannParameters.ShearStress[input.Bob.ToString()]
                );

            IRheoModel model = null;
            switch(id)
            {
                case 1:
                    model = new NewtonModel(mes);
                    break;
                case 2:
                    model = new BinghamModel(mes);
                    break;
                case 3:
                    model = new CassonModel(mes);
                    break;
                case 4:
                    model = new OstwaldDeWaeleModel(mes);
                    break;
                default:
                    break;
            }

            if (model == null)
                return null;


            RheoModelViewModel output = new RheoModelViewModel(){
                    Id = id,
                    Name = model.Name,
                    R = model.R,
                    F = model.F,
                    U = model.U,
                    ShearStress = model.ModeledStress.ToArray(),
                    SpecificParameters = model.SpecificParams
                };
            return output;
        }



    }
}