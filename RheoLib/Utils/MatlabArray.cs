using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace RheoLib.Utils
{
    public class MatlabArray : IEnumerable
    {
        private double[] array;
        public int Count{get; private set;}

        public double this[int index]
        {
            get{ return array[index];}
            set{ array[index] = value;}
        }

        public MatlabArray(int size)
        {
            if(size <= 0)
                throw new ArgumentException("Size must be greater than 0");
            array = new double[size];
            Count = size;
        }
        public MatlabArray(MatlabArray arr)
        {
            if(arr == null)
                throw new NullReferenceException("Provide initialized MatlabArray");
            array = arr.array;
            Count = arr.Count;
        }
        public MatlabArray(double[] arr)
        {
            if(arr == null)
                throw new NullReferenceException("Provide initialized double[]");
            if(arr.Length ==0)
                throw new ArgumentException("Provide correct double[] array");

            array = arr;
            Count = arr.Length;
        }
        public MatlabArray(int[] arr)
        {
            if(arr == null)
                throw new NullReferenceException("Provide initialized int[]");
            if(arr.Length ==0)
                throw new ArgumentException("Provide correct int[] array");

            Count = arr.Length;
            array = new double[Count];
            for(int i = 0; i < Count ; i++)
            {
                array[i] = arr[i];
            }
        }
        public void FillRandom(int min, int max)
        {
            Random rnd = new Random();
            for (int i = 0; i<Count; i++)
            {
                array[i] = rnd.Next(min,max);
            }
        }

        public void Fill(double value)
        {
            Random rnd = new Random();
            for (int i = 0; i<Count; i++)
            {
                array[i] = value;
            }
        }

        public static double Sum(MatlabArray arr)
        {
            if(arr == null)
                throw new NullReferenceException("Provide initialized MatlabArray");

            if(arr.Count == 0)
                return 0;

            double result = 0;
            for (int i = 0 ; i < arr.Count; i++)
            {
                result+=arr[i];
            }
            return result;
        }
        public static double Mean(MatlabArray arr)
        {
            if(arr == null)
                throw new NullReferenceException("Provide initialized MatlabArray");

            if(arr.Count == 0)
                return 0;

            double result = 0;
            for (int i = 0 ; i < arr.Count; i++)
            {
                result+=arr[i];
            }
            return 1.0*result/arr.Count;
        }
        public  double Max()
        {
            return array.Max();
        }
        public  double Min()
        {
            return array.Min();
        }


        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0 ; i < Count; i++)
            {
                str.Append(array[i] + " ");
            }
            return str.ToString();
        }

        public double[] ToArray()
        {
            double[] result = new double[Count];
            for (int i = 0 ; i < Count; i++)
            {
                result[i] = array[i];
            }
            return result;
        }

        //Math operators
        public static MatlabArray operator+ (MatlabArray a, MatlabArray b)
        {
            if(a == null || b == null)
                throw new NullReferenceException("Provide initialized MatlabArrays");

            if(a.Count == 0 || b.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");

            if(a.Count != b.Count)
                throw new ArgumentException("Arrays size differs");

            MatlabArray result = new MatlabArray(a.Count);
            for (int i = 0; i<a.Count; i++)
            {
                result[i] = a[i] + b[i];
            }
            return result;
        }
        public static MatlabArray operator+ (MatlabArray a, double b)
        {
            if(a == null)
                throw new NullReferenceException("Provide initialized MatlabArray");

            if(a.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");


            MatlabArray result = new MatlabArray(a.Count);
            for (int i = 0; i<a.Count; i++)
            {
                result[i] = a[i] + b;
            }
            return result;
        }
        public static MatlabArray operator+ (double a, MatlabArray b)
        {
            if(b == null)
                throw new NullReferenceException("Provide initialized MatlabArrays");

            if(b.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");

            MatlabArray result = new MatlabArray(b.Count);
            for (int i = 0; i<b.Count; i++)
            {
                result[i] = a + b[i];
            }
            return result;
        }
        public static MatlabArray operator- (MatlabArray a, MatlabArray b)
        {
            if(a == null || b == null)
                throw new NullReferenceException("Provide initialized MatlabArrays");

            if(a.Count == 0 || b.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");

            if(a.Count != b.Count)
                throw new ArgumentException("Arrays size differs");

            MatlabArray result = new MatlabArray(a.Count);
            for (int i = 0; i<a.Count; i++)
            {
                result[i] = a[i]-b[i];
            }
            return result;
        }
        public static MatlabArray operator- (MatlabArray a, double b)
        {
            if(a == null)
                throw new NullReferenceException("Provide initialized MatlabArray");

            if(a.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");


            MatlabArray result = new MatlabArray(a.Count);
            for (int i = 0; i<a.Count; i++)
            {
                result[i] = a[i]-b;
            }
            return result;
        }
        public static MatlabArray operator- (double a, MatlabArray b)
        {
            if(b == null)
                throw new NullReferenceException("Provide initialized MatlabArrays");

            if(b.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");

            MatlabArray result = new MatlabArray(b.Count);
            for (int i = 0; i<b.Count; i++)
            {
                result[i] = a - b[i];
            }
            return result;
        }
        public static MatlabArray operator* (MatlabArray a, MatlabArray b)
        {
            if(a == null || b == null)
                throw new NullReferenceException("Provide initialized MatlabArrays");

            if(a.Count == 0 || b.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");

            if(a.Count != b.Count)
                throw new ArgumentException("Arrays size differs");

            MatlabArray result = new MatlabArray(a.Count);
            for (int i = 0; i<a.Count; i++)
            {
                result[i] = a[i]*b[i];
            }
            return result;
        }
        public static MatlabArray operator* (MatlabArray a, double b)
        {
            if(a == null)
                throw new NullReferenceException("Provide initialized MatlabArray");

            if(a.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");

            MatlabArray result = new MatlabArray(a.Count);
            for (int i = 0; i<a.Count; i++)
            {
                result[i] = a[i]*b;
            }
            return result;
        }
        public static MatlabArray operator* (double a, MatlabArray b)
        {
            if(b == null)
                throw new NullReferenceException("Provide initialized MatlabArrays");

            if(b.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");

            MatlabArray result = new MatlabArray(b.Count);
            for (int i = 0; i<b.Count; i++)
            {
                result[i] = a * b[i];
            }
            return result;
        }
        public static MatlabArray operator/ (MatlabArray a, MatlabArray b)
        {
             if(a == null || b == null)
                throw new NullReferenceException("Provide initialized MatlabArrays");

            if(a.Count == 0 || b.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");

            if(a.Count != b.Count)
                throw new ArgumentException("Arrays size differs");

            MatlabArray result = new MatlabArray(a.Count);
            for (int i = 0; i<a.Count; i++)
            {
                if (b[i] == 0 )
                {
                    throw new DivideByZeroException();
                }

                result[i] = a[i]/b[i];
            }
            return result;
        }
        public static MatlabArray operator/ (MatlabArray a, double b)
        {
            if(a == null)
                throw new NullReferenceException("Provide initialized MatlabArray");

            if(a.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");

            if (b == 0 )
                throw new DivideByZeroException();

            MatlabArray result = new MatlabArray(a.Count);
            for (int i = 0; i<a.Count; i++)
            {
                result[i] = a[i]/b;
            }
            return result;
        }
        public static MatlabArray operator/ (double a, MatlabArray b)
        {
            if(b == null)
                throw new NullReferenceException("Provide initialized MatlabArrays");

            if(b.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");

            MatlabArray result = new MatlabArray(b.Count);
            for (int i = 0; i<b.Count; i++)
            {
                if (b[i] == 0 )
                {
                    throw new DivideByZeroException();
                }
                result[i] = a/b[i];
            }
            return result;
        }
        //Warning: Operator ^ priority == */ operators priority
        public static MatlabArray operator^ (MatlabArray a, double b)
        {
            if(a == null)
                throw new NullReferenceException("Provide initialized MatlabArray");

            if(a.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");

            MatlabArray result = new MatlabArray(a.Count);
            for (int i = 0; i<a.Count; i++)
            {
                result[i] = Math.Pow(a[i],b);
            }
            return result;
        }

        public static MatlabArray Logn(MatlabArray arr)
        {
            if(arr == null)
                throw new NullReferenceException("Provide initialized MatlabArray");
            if(arr.Count == 0)
                throw new ArgumentException("Arrays count must be > 0");

            MatlabArray result = new MatlabArray(arr.Count);
            for(int i = 0 ; i<arr.Count ; i++)
            {
                result[i] = Math.Log(arr[i], Math.E);
            }
            return result;
        }
        //IEnumerable interface implementation
        public IEnumerator GetEnumerator()
        {
            return array.GetEnumerator();
        }
    }
}