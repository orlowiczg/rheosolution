using System;

namespace RheoLib.Utils
{
    public static class Regression
    {
        //Regression parameters
        //U - sum of squares
        //R - Pearson correlation coefficient
        //F - Fisher - Snedecor coefficient

        public static double CalcU(MatlabArray MeasuredStress, MatlabArray ModeledStress)
        {
            return MatlabArray.Sum((MeasuredStress-ModeledStress)^2);
        }
        public static double CalcR(MatlabArray MeasuredStress, MatlabArray ModeledStress, double SquaresSum)
        {
            double Rtemp =  (1-((SquaresSum)/(MatlabArray.Sum((MeasuredStress - MatlabArray.Mean(MeasuredStress))^2))));

            if(Rtemp <= 0)
                return 0;

            return Math.Pow(Rtemp,0.5);
        }
        public static double CalcF(double R, double m)
        {
            return (R*R)/(1-R*R)*(m-2)/(1);
        }
    }
}