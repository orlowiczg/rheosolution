using System;
using System.Collections.Generic;
using RheoLib.Utils;

namespace RheoLib.RheoModels
{
    public class HerschelBurkleyModel : IRheoModel
    {
        public HerschelBurkleyModel(Measurement measuredProperties)
        {
            Name = "Herschel Burkley";
            MeasuredProp = measuredProperties;
        }

        public string Name { get; set; }
        public double R { get; private set;}
        public double F  { get; private set;}
        public double U  { get; private set; }
        public Measurement MeasuredProp {get;}
        public MatlabArray ModeledStress { get; private set;}

        //Collection with currents model specific factors
        public Dictionary<string,double> SpecificParams { get; private set;} = new Dictionary<string, double>();

        private void Calculate()
        {
            //temporary vars for better  readability
            MatlabArray Y = MeasuredProp.ShearRate;
            MatlabArray Ytemp = MatlabArray.Logn(MeasuredProp.ShearRate);
            MatlabArray t = MeasuredProp.ShearStress;
            double m = MeasuredProp.SeriesNumber;  
            
            //vars for binary search
            double a = 0;
            double b = t.Max();
            double epsilon = 1;
            double RTemp = 0;
            double ty0 = (a+b)/2.0;
            
            //specific factors from regression equations
            
            //TODO


            //add specific params to dict

            //TODO
      
           
            //regresion parameters for model evaluation
            U = Regression.CalcU(MeasuredProp.ShearStress, ModeledStress);
            R = Regression.CalcR(MeasuredProp.ShearStress, ModeledStress, U);
            F = Regression.CalcF(R, MeasuredProp.SeriesNumber);

        }
        //tuple c# 7.0 syntax
        public (double FBI, double CI, double ty0, double R) HBEquation(MatlabArray t, MatlabArray Y, double m, double ty0)
        {
            MatlabArray Ytemp = MatlabArray.Logn(Y);
            MatlabArray ttemp = MatlabArray.Logn(t-ty0);

            double FlowBehaviorIndex = (m*MatlabArray.Sum(Ytemp*ttemp)-MatlabArray.Sum(Ytemp)*MatlabArray.Sum(ttemp))/
            (m*MatlabArray.Sum(Ytemp*Ytemp)-(Math.Pow(MatlabArray.Sum(Ytemp),2)));
            double ConsistencyIndex = Math.Exp((MatlabArray.Sum(ttemp)-FlowBehaviorIndex*MatlabArray.Sum(Ytemp))/(m));
            
            MatlabArray thb = ty0 + ConsistencyIndex*(Y^FlowBehaviorIndex);
            double U = Regression.CalcU(t,thb);
            double R = Regression.CalcR(t,thb,U);

            return (FlowBehaviorIndex, ConsistencyIndex, ty0, R); 
        }
    }
}