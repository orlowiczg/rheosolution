using RheoLib.Utils;
using System;
using System.Collections.Generic;

namespace RheoLib.RheoModels
{
    public class NewtonModel : IRheoModel
    {
        public NewtonModel(Measurement measuredProperties)
        {
            Name = "Newton";
            MeasuredProp = measuredProperties;
            Calculate();
        }

        public string Name { get; set; }
        public double R { get; private set;}
        public double F  { get; private set;}
        public double U  { get; private set; }
        public Measurement MeasuredProp {get;}
        public MatlabArray ModeledStress { get; private set;}

        //Collection with currents model specific factors
        public Dictionary<string,double> SpecificParams { get; private set;} = new Dictionary<string, double>();

        private void Calculate()
        {
            //specific factors from regression equations
            double DynamicViscosity = MatlabArray.Sum(MeasuredProp.ShearRate*MeasuredProp.ShearStress)/
            MatlabArray.Sum(MeasuredProp.ShearRate*MeasuredProp.ShearRate);

            ModeledStress = DynamicViscosity * MeasuredProp.ShearRate;

            //add specific params to dict
            SpecificParams.Add("Dynamic Viscosity", DynamicViscosity);

            //regresion parameters for model evaluation
            U = Regression.CalcU(MeasuredProp.ShearStress, ModeledStress);
            R = Regression.CalcR(MeasuredProp.ShearStress, ModeledStress, U);
            F = Regression.CalcF(R, MeasuredProp.SeriesNumber);
        }






    }
}