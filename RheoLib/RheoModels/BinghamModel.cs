using RheoLib.Utils;
using System;
using System.Collections.Generic;

namespace RheoLib.RheoModels
{
    public class BinghamModel : IRheoModel
    {
        public BinghamModel(Measurement measuredProperties)
        {
            Name = "Bingham";
            MeasuredProp = measuredProperties;
            Calculate();
        }

        public string Name { get; set; }
        public double R { get; private set;}
        public double F  { get; private set;}
        public double U  { get; private set; }
        public Measurement MeasuredProp {get;}
        public MatlabArray ModeledStress { get; private set;}

        //Collection with currents model specific factors
        public Dictionary<string,double> SpecificParams { get; private set;} = new Dictionary<string, double>();

        private void Calculate()
        {

            //specific factors from regression equations
            double PlasticViscosity = (MeasuredProp.SeriesNumber*MatlabArray.Sum(MeasuredProp.ShearRate*MeasuredProp.ShearStress) 
            - MatlabArray.Sum(MeasuredProp.ShearRate)*MatlabArray.Sum(MeasuredProp.ShearStress))/
            (MeasuredProp.SeriesNumber*MatlabArray.Sum(MeasuredProp.ShearRate*MeasuredProp.ShearRate)
            - (Math.Pow(MatlabArray.Sum(MeasuredProp.ShearRate),2)));
            
            double YieldPoint = (MatlabArray.Sum(MeasuredProp.ShearStress)-PlasticViscosity*MatlabArray.Sum(MeasuredProp.ShearRate))/
            (MeasuredProp.SeriesNumber);

            //add specific params to dict
            SpecificParams.Add("Plastic Viscosity", PlasticViscosity);
            SpecificParams.Add("Yield Point", YieldPoint);

            //Calculated Shear Stress from Bingham Model Equation t = ty + n * t
            ModeledStress = YieldPoint + PlasticViscosity * MeasuredProp.ShearRate;
            
            //regresion parameters for model evaluation
            U = Regression.CalcU(MeasuredProp.ShearStress, ModeledStress);
            R = Regression.CalcR(MeasuredProp.ShearStress, ModeledStress, U);
            F = Regression.CalcF(R, MeasuredProp.SeriesNumber);
        }






    }
}