using System.Collections.Generic;
using RheoLib.Utils;
using System;

namespace RheoLib.RheoModels
{
    public class CassonModel : IRheoModel
    {
        public CassonModel(Measurement measuredProperties)
        {
            Name = "Casson";
            MeasuredProp = measuredProperties;
            Calculate();
        }

        public string Name { get; set; }
        public double R { get; private set;}
        public double F  { get; private set;}
        public double U  { get; private set; }
        public Measurement MeasuredProp {get;}
        public MatlabArray ModeledStress { get; private set;}

        //Collection with currents model specific factors
        public Dictionary<string,double> SpecificParams { get; private set;} = new Dictionary<string, double>();

        private void Calculate()
        {
            MatlabArray Ytemp = MeasuredProp.ShearRate^0.5;
            MatlabArray ttemp = MeasuredProp.ShearStress^0.5;

            //specific factors from regression equations
            double CassonViscosity = (MeasuredProp.SeriesNumber*MatlabArray.Sum(Ytemp*ttemp)-
            MatlabArray.Sum(Ytemp)*MatlabArray.Sum(ttemp))/(MeasuredProp.SeriesNumber*
            MatlabArray.Sum(Ytemp*Ytemp)-(Math.Pow(MatlabArray.Sum(Ytemp),2)));
            double YieldPoint =(MatlabArray.Sum(ttemp)-CassonViscosity*MatlabArray.Sum(Ytemp))/(MeasuredProp.SeriesNumber);

            CassonViscosity = Math.Pow(CassonViscosity,2);
            YieldPoint = Math.Pow(YieldPoint,2);

            ModeledStress = (Math.Pow(YieldPoint, 0.5) + ((CassonViscosity * MeasuredProp.ShearRate)^0.5))^2;

            //add specific params to dict
            SpecificParams.Add("Casson Viscosity", CassonViscosity);
            SpecificParams.Add("Yield Point", YieldPoint);

            //regresion parameters for model evaluation
            U = Regression.CalcU(MeasuredProp.ShearStress, ModeledStress);
            R = Regression.CalcR(MeasuredProp.ShearStress, ModeledStress, U);
            F = Regression.CalcF(R, MeasuredProp.SeriesNumber);
       
        }
    }
}