using System.Collections.Generic;
using RheoLib.Utils;

namespace RheoLib.RheoModels
{
    public interface IRheoModel
    {
        string Name { get; set; }
        double R { get; }
        double F { get; }
        double U { get; }
        Measurement MeasuredProp { get; }
        MatlabArray ModeledStress { get; }
        Dictionary<string,double> SpecificParams {get;}
    }
}