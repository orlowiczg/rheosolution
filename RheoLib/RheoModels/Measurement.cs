using System.Collections.Generic;
using RheoLib.Utils;
using System;


namespace RheoLib.RheoModels
{
    public class Measurement
    {
        public Measurement(double[] angle, int[] rate, double rotorRadius, double bobRadius, double springConst, double springStress)
        {
            Angle = angle;
            RotSpeed = rate;
            SeriesNumber = angle.Length;
            RotorRadius = rotorRadius;
            BobRadius = bobRadius;
            SpringConst = springConst;
            SpringStress = springStress;
            Calculate();

        }

        public int[] RotSpeed {get;}
        public double[] Angle {get;}
        public double SeriesNumber { get; }
        public double RotorRadius{ get; }
        public double BobRadius { get;  }
        public double SpringConst { get; }
        public double SpringStress { get; }

        //---------------------------------------------------------------------

        public MatlabArray ShearStress {get; private set;}
        public MatlabArray ShearRate {get; private set;}
        public MatlabArray ApparentViscosity {get; private set;}

        public void Calculate()
        {
            if(RotSpeed.Length != Angle.Length)
                throw new Exception("Rate and Angle arrays length have to be equal");

            //ShearStress calculations
            ShearStress = new MatlabArray(Angle);
            ShearStress = ShearStress*SpringConst*SpringStress;

            //ShearRate calculations
            double rat = BobRadius/RotorRadius;
            ShearRate = new MatlabArray(RotSpeed);
            ShearRate = ShearRate*2.0*2.0*Math.PI/60.0/(1-rat*rat);

            //Apparentt Viscosity
            ApparentViscosity = ShearStress/ShearRate;
        }
    }
}