using System.Collections.Generic;
using RheoLib.Utils;
using System;
namespace RheoLib.RheoModels
{
    public class OstwaldDeWaeleModel : IRheoModel
    {
        public OstwaldDeWaeleModel(Measurement measuredProperties)
        {
            Name = "Ostwald de Waele";
            MeasuredProp = measuredProperties;
            Calculate();
        }

        public string Name { get; set; }
        public double R { get; private set;}
        public double F  { get; private set;}
        public double U  { get; private set; }
        public Measurement MeasuredProp {get;}
        public MatlabArray ModeledStress { get; private set;}

        //Collection with currents model specific factors
        public Dictionary<string,double> SpecificParams { get; private set;} = new Dictionary<string, double>();

        private void Calculate()
        {
            //temporary vars for better  readability
            MatlabArray Y = MeasuredProp.ShearRate;
            MatlabArray t = MeasuredProp.ShearStress;
            double m = MeasuredProp.SeriesNumber;  

            //specific factors from regression equations

            double BehaviorFlowIndex = (m *MatlabArray.Sum(MatlabArray.Logn(Y)*MatlabArray.Logn(t)) 
            - MatlabArray.Sum(MatlabArray.Logn(Y)) * MatlabArray.Sum(MatlabArray.Logn(t))) / 
            (m*MatlabArray.Sum(MatlabArray.Logn(Y)^2) - (Math.Pow(MatlabArray.Sum(MatlabArray.Logn(Y)),2)));
            
            double ConsistencyIndex = Math.Exp((MatlabArray.Sum(MatlabArray.Logn(t)) 
            - BehaviorFlowIndex*MatlabArray.Sum(MatlabArray.Logn(Y)))/m);

            ModeledStress = ConsistencyIndex * (Y^BehaviorFlowIndex);

            //add specific params to dict
            SpecificParams.Add("Behavior Flow Index", BehaviorFlowIndex);
            SpecificParams.Add("Consistency Index", ConsistencyIndex);
           
            //regresion parameters for model evaluation
            U = Regression.CalcU(MeasuredProp.ShearStress, ModeledStress);
            R = Regression.CalcR(MeasuredProp.ShearStress, ModeledStress, U);
            F = Regression.CalcF(R, MeasuredProp.SeriesNumber);

        }
    }
}