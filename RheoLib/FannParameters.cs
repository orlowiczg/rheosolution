using System.Collections.Generic;

namespace RheoLib
{
    public static class FannParameters
    {
        public static Dictionary<string, double> CyllinderRadius {get;} = new Dictionary<string, double>
        {
            {"B1",1.7245},{"B2",1.2276}, {"B3",0.86225}, {"B4",0.86225}, {"R1",1.8415}, {"R2",1.7589}, {"R3",2.5867}
        };

        public static Dictionary<string, double> SpringType {get;} = new Dictionary<string, double>
        {
            {"F02",0.2}, {"F05",0.5}, {"F1",1}, {"F2",2}, {"F3",3}, {"F4",4}, {"F5",5}, {"F10",10}
        };

        //naprężenia styczne przypadające na 1st kata skrecenia sprezyny
        public static Dictionary<string, double> ShearStress {get;} = new Dictionary<string, double>
        {
            {"B1",0.511}, {"B2",1.009}, {"B3",2.045}, {"B4",4.092}
        };
    }
}