namespace RheoSolutionClient.ViewModels
{
    public class MeasurementInputVM
    {
        public MeasurementViewModel Measurement {get; set;}
        public bool[] Selected {get; set;} = new bool[12];
    }
}