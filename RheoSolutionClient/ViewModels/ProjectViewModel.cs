using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using RheoSolutionClient.Models;

namespace RheoSolutionClient.ViewModels
{

    public class ProjectViewModel
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        [MaxLength(250)]
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string OwnerId { get; set;}
        public List<Measurement> Measurements { get; set; }

    }
}