using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Newtonsoft.Json;
using RheoSolutionClient.Globals;
using RheoSolutionClient.Models;
using RheoSolutionClient.Validations;

namespace RheoSolutionClient.ViewModels
{
    public class MeasurementViewModel
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        [MaxLength(250)]
        public string Description { get; set; }

        [Required]
        public BobsEnum Bob {get; set;}
        [Required]
        public RotorsEnum Rotor {get; set;}
        [Required]
        public SpringsEnum Spring {get; set;}

        [Required]
        [DictionaryPositiveElements]
        public Dictionary<int,double> SpeedAngle { get; set; } = new Dictionary<int, double>() { {1,0}, {2,0}, {3,0}, {6,0}, {10,0}, {20,0}, {30,0}, {60,0}, {100,0}, {200,0}, {300,0}, {600,0}};
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public RheoTotalViewModel Data {get; set;}
    }

}