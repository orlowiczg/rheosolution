using System.Collections.Generic;
using RheoSolutionClient.ApiModels;
using RheoSolutionClient.Globals;

namespace RheoSolutionClient.ViewModels
{
    public class RheoTotalViewModel
    {
        public  RheologyResponseModel Rheology {get;set;}
        public List<RheoModelResponseModel> Models {get;set;}
        public BobsEnum Bob {get; set;}
        public RotorsEnum Rotor {get; set;}
        public SpringsEnum Spring {get; set;}
    }
}