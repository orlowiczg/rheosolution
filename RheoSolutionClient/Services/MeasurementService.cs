using System;
using System.Linq;
using RheoSolutionClient.Data;
using RheoSolutionClient.Models;

namespace RheoSolutionClient.Services
{
    public class MeasurementService
    {
        private AppDbContext _ctx;
        public MeasurementService(AppDbContext ctx)
        {
            _ctx = ctx;
        }


        public int AddMeasurement(Measurement measurement)
        {
            measurement.Created = DateTime.Now;
            measurement.Modified = DateTime.Now;

            _ctx.Measurements.Add(measurement);
            _ctx.SaveChanges();
            return measurement.Id;
        }

        public Measurement GetMeasurementById(int id)
        {
            Measurement result = _ctx.Measurements.Where( m => m.Id == id).FirstOrDefault();
            result.Project = _ctx.Projects.Where( p => p.Id == result.ProjectId).FirstOrDefault();
            return result;
        }

        public void DeleteMeasurement(int id)
        {
            Measurement toDelete = GetMeasurementById(id);
            if(toDelete == null)
            {
                return;
            }
            _ctx.Measurements.Remove(toDelete);
            _ctx.SaveChanges();

        }




    }
}