using System.Collections.Generic;
using RheoSolutionClient.Models;

namespace RheoSolutionClient.Services
{
    public interface IProjectService
    {
        void AddProject(Project project);
        void EditProject(Project project);
        void DeleteProject(int id);
        List<Project> GetAllProjects();
        Project GetProjectById(int id);
        List<Measurement> GetMeasurementsByProjectId(int id);
    }
}