using System.Collections.Generic;
using System.Threading.Tasks;
using RheoSolutionClient.ApiModels;

namespace RheoSolutionClient.Services
{
    public interface IRheoService
    {
        Task<RheologyResponseModel> GetRheologyAsync(RequestModel input);
        Task<RheoModelResponseModel> GetRheoModelAsync(RequestModel input, int id);
        Task<List<RheoModelResponseModel>> GetAllRheoModelsAsync(RequestModel input);
    }
}