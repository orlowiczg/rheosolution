using System.Net.Http;
using System;
using System.Threading.Tasks;
using RheoSolutionClient.ApiModels;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Generic;

namespace RheoSolutionClient.Services
{
    public class RheoSolutionApiService : IRheoService
    {
        private readonly HttpClient _client;

        public RheoSolutionApiService(HttpClient client)
        {
            _client = client;
            _client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<RheologyResponseModel> GetRheologyAsync(RequestModel input)
        {
            string json = JsonConvert.SerializeObject(input);        
            HttpResponseMessage response = await _client.PostAsync(_client.BaseAddress + "rheology",new StringContent(json, Encoding.UTF8, "application/json"));

            if(response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<RheologyResponseModel>();
            }
            return  new RheologyResponseModel();
        }

        public async Task<RheoModelResponseModel> GetRheoModelAsync(RequestModel input, int id)
        {
            string json = JsonConvert.SerializeObject(input);        
            HttpResponseMessage response = await _client.PostAsync(_client.BaseAddress + "rheomodel/" + id,new StringContent(json, Encoding.UTF8, "application/json"));

            if(response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<RheoModelResponseModel>();
            }
            return  new RheoModelResponseModel();
        }

        public async Task<List<RheoModelResponseModel>> GetAllRheoModelsAsync(RequestModel input)
        {
            string json = JsonConvert.SerializeObject(input);        
            HttpResponseMessage response = await _client.PostAsync(_client.BaseAddress + "rheomodel",new StringContent(json, Encoding.UTF8, "application/json"));

            if(response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<List<RheoModelResponseModel>>();
            }
            return  new List<RheoModelResponseModel>();
        }
    }
}