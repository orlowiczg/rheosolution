using System.Collections.Generic;
using System.Linq;
using RheoSolutionClient.Data;
using RheoSolutionClient.Models;
using System;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;

namespace RheoSolutionClient.Services
{
    public class ProjectService : IProjectService
    {
        private AppDbContext _ctx;
        private UserManager<IdentityUser> _userManager;
        private IHttpContextAccessor _httpContext;
        public ProjectService(AppDbContext ctx, UserManager<IdentityUser> userManager, IHttpContextAccessor httpContext)
        {
            _ctx = ctx;
            _userManager = userManager;
            _httpContext = httpContext;

        }


        public void AddProject(Project project)
        {
            project.Created = DateTime.Now;
            project.Modified = DateTime.Now;
            project.Measurements = new List<Measurement>();
            project.OwnerId = _userManager.GetUserId(_httpContext.HttpContext.User);
            _ctx.Projects.Add(project);
            _ctx.SaveChanges();

        }

        public void DeleteProject(int id)
        {
            Project toDelete = GetProjectById(id);

            if(toDelete == null)
                return;
            _ctx.Projects.Remove(toDelete);
            _ctx.SaveChangesAsync();
        }

        public void EditProject(Project project)
        {
            Project toUpdate = GetProjectById(project.Id);
            toUpdate.Name = project.Name;
            toUpdate.Description = project.Description;
            toUpdate.Modified = DateTime.Now;
            _ctx.Projects.Update(toUpdate);
            _ctx.SaveChangesAsync();
        }

        public List<Project> GetAllProjects()
        {
            return _ctx.Projects.Where(p => p.OwnerId == _userManager.GetUserId(_httpContext.HttpContext.User)).ToList();
        }

        public List<Measurement> GetMeasurementsByProjectId(int projectId)
        {
            return _ctx.Measurements.Where(m => m.ProjectId == projectId).ToList();
        }

        public Project GetProjectById(int id)
        {
            return _ctx.Projects.Where(p => p.Id == id).FirstOrDefault();
        }
    }
}