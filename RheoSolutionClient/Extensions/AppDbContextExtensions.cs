using System;
using System.Collections.Generic;
using System.Linq;
using RheoSolutionClient.Data;
using RheoSolutionClient.Globals;
using RheoSolutionClient.Models;

namespace RheoSolutionClient.Extensions
{
    public static class AppDbContextExtensions
    {
        public static void SeedData(this AppDbContext context)
        {
            if(context.Projects.Any())
            {
                return;
            }

            var projects = new List<Project>()
            {
                new Project()
                {
                    Name = "Zlecenie nr 954/6 ",
                    Description = "Projekt wykonywany dla firmy PGNiG Krakow",
                    Created = DateTime.Now,
                    Modified = DateTime.Now,

                    Measurements = new List<Measurement>()
                    {
                        new Measurement()
                        {
                            Name = "Pomiar nr 1",
                            Description = "Pomiar parametrow pluczki bez dodatkow",
                            SpeedAngle = new Dictionary<int, double>()
                            {
                                 {1,2}, {2,2}, {3,3}, {6,4}, {10,6}, {20,10}, {30,14}, {60,28}, {100,48}, {200,91}, {300,141}, {600,237}
                            },
                            Spring = SpringsEnum.F1,
                            Bob  =BobsEnum.B1,
                            Rotor = RotorsEnum.R1,
                            Created = DateTime.Now,
                            Modified = DateTime.Now,


                        },
                        new Measurement()
                        {
                            Name = "Pomiar nr 2",
                            Description = "Pomiar parametrow pluczki bez dodatkow",
                            SpeedAngle = new Dictionary<int, double>()
                            {
                                 {1,2}, {2,2}, {3,4}, {6,5}, {10,7}, {20,10}, {30,14}, {60,28}, {100,48}, {200,91}, {300,141}, {600,237}
                            },
                            Spring = SpringsEnum.F1,
                            Bob  =BobsEnum.B1,
                            Rotor = RotorsEnum.R1,
                            Created = DateTime.Now,
                            Modified = DateTime.Now,



                        }
                    }
                },

                new Project()
                {
                    Name = "Zlecenie nr 954/7 ",
                    Description = "Projekt wykonywany dla firmy PGNiG Warszawa",
                    Created = DateTime.Now,
                    Modified = DateTime.Now,

                    Measurements = new List<Measurement>()
                    {
                        new Measurement()
                        {
                            Name = "Pomiar nr 1",
                            Description = "Pomiar parametrow pluczki bez dodatkow",
                            SpeedAngle = new Dictionary<int, double>()
                            {
                                 {1,2}, {2,2}, {3,3}, {6,4}, {10,6}, {20,10}, {30,14}, {60,28}, {100,48}, {200,91}, {300,141}, {600,237}
                            },
                            Spring = SpringsEnum.F1,
                            Bob  =BobsEnum.B1,
                            Rotor = RotorsEnum.R1,
                            Created = DateTime.Now,
                            Modified = DateTime.Now,


                        },
                        new Measurement()
                        {
                            Name = "Pomiar nr 2",
                            Description = "Pomiar parametrow pluczki bez dodatkow",
                            SpeedAngle = new Dictionary<int, double>()
                            {
                                 {1,2}, {2,2}, {3,4}, {6,5}, {10,7}, {20,10}, {30,14}, {60,28}, {100,48}, {200,91}, {300,141}, {600,237}
                            },
                            Spring = SpringsEnum.F1,
                            Bob  =BobsEnum.B1,
                            Rotor = RotorsEnum.R1,
                            Created = DateTime.Now,
                            Modified = DateTime.Now,

                        }
                    }
                }
            };
            context.Projects.AddRange(projects);
            context.SaveChanges();
        }
    }
}