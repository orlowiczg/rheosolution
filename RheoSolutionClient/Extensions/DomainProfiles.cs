using AutoMapper;
using RheoSolutionClient.Models;
using RheoSolutionClient.ViewModels;

namespace RheoSolutionClient.Extensions
{

    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<Project, ProjectViewModel>();
            CreateMap<ProjectViewModel, Project>();
            CreateMap<MeasurementViewModel, Measurement>();
            CreateMap<Measurement, MeasurementViewModel>();


        }
    }
}