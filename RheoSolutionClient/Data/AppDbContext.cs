using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using RheoSolutionClient.Models;

namespace RheoSolutionClient.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext( DbContextOptions options) : base(options)
        {
            Database.Migrate();
        }
        public DbSet<Project>  Projects { get; set; }
        public DbSet<Measurement>  Measurements { get; set; }
    }
}