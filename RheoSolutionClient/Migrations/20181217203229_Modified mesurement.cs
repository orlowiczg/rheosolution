﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RheoSolutionClient.Migrations
{
    public partial class Modifiedmesurement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Bob",
                table: "Measurements",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Rotor",
                table: "Measurements",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Spring",
                table: "Measurements",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Bob",
                table: "Measurements");

            migrationBuilder.DropColumn(
                name: "Rotor",
                table: "Measurements");

            migrationBuilder.DropColumn(
                name: "Spring",
                table: "Measurements");
        }
    }
}
