using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RheoSolutionClient.Areas.Identity.Data;

[assembly: HostingStartup(typeof(RheoSolutionClient.Areas.Identity.IdentityHostingStartup))]
namespace RheoSolutionClient.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<RheoSolutionClientIdentityDbContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("RheoSolutionClientIdentityDbContextConnection")));

                services.AddTransient<UserManager<IdentityUser>>();
                services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

                services.AddDefaultIdentity<IdentityUser>()
                    .AddEntityFrameworkStores<RheoSolutionClientIdentityDbContext>();
            });
        }
    }
}