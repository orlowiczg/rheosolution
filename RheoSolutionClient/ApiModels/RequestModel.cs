using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using RheoSolutionClient.Globals;

namespace RheoSolutionClient.ApiModels
{
    public class RequestModel
    {
         public int[] Rate {get; set;} = new int[12]{1,2,3,6,10,20,30,60,100,200,300,600};

        [Required]
        public double[] Angle {get; set;} = new double[12]{0,0,0,0,0,0,0,0,0,0,0,0};

        public bool[] Selected { get; set; } = new bool[12]{false,false,false,false,false,false,false,false,false,false,false,false};

        [Required]
        public BobsEnum Bob {get; set;}

        [Required]
        public RotorsEnum Rotor {get; set;}

        [Required]
        public SpringsEnum Spring {get; set;}
    }
}