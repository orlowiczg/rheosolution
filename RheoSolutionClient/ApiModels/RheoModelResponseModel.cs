using System.Collections.Generic;

namespace RheoSolutionClient.ApiModels
{
    public class RheoModelResponseModel
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public double R { get; set; }   
        public double F { get; set; }
        public double U { get; set; } 
        public Dictionary<string,double> SpecificParameters { get; set; }
        public double[] ShearStress { get; set; }
    }
}