namespace RheoSolutionClient.ApiModels
{
    public class RheologyResponseModel
    {
        public int[] RotSpeed {get;set;}
        public double[] Angle { get; set; }
        public double[] ShearStress {get; set;}
        public double[] ShearRate { get; set; }   
        public double[] ApparentViscosity { get; set; }
    }
}