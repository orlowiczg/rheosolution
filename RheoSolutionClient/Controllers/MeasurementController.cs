using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RheoSolutionClient.ApiModels;
using RheoSolutionClient.Models;
using RheoSolutionClient.Services;
using RheoSolutionClient.ViewModels;

namespace RheoSolutionClient.Controllers
{
    public class MeasurementController : Controller
    {

        private MeasurementService _mservice;
        private IMapper _mapper;
        private  RheoSolutionApiService _apiclient;

        public MeasurementController(MeasurementService mservice, IMapper mapper, RheoSolutionApiService apiclient)
        {
            _apiclient = apiclient;
            _mservice = mservice;
            _mapper = mapper;
        }



        public IActionResult Index(int id)
        {

            Measurement model = _mservice.GetMeasurementById(id);
            RequestModel request = new RequestModel(){
                Bob = model.Bob,
                Rotor = model.Rotor,
                Spring = model.Spring,
                Angle = model.SpeedAngle.Values.ToArray(),
                Rate = model.SpeedAngle.Keys.ToArray()
            };

            RheologyResponseModel rheology =  _apiclient.GetRheologyAsync(request).Result;
            List<RheoModelResponseModel> models = _apiclient.GetAllRheoModelsAsync(request).Result;

            RheoTotalViewModel rheoTotal = new RheoTotalViewModel(){
                Models = models,
                Rheology = rheology,
                Bob = request.Bob,
                Rotor = request.Rotor,
                Spring = request.Spring,
            };

            MeasurementViewModel result = _mapper.Map<MeasurementViewModel>(model);
            result.Data = rheoTotal;



            return View(result);
        }

        [HttpGet]
        public IActionResult Create()
        {
            int[] rate = new int[12]{1,2,3,6,10,20,30,60,100,200,300,600};
            ViewBag.rate = rate;
            return View();
        }
        [HttpPost]
        public IActionResult Create(MeasurementInputVM model, int id)
        {
            MeasurementViewModel measurement = model.Measurement;
            measurement.ProjectId = id;

            Measurement result = _mapper.Map<Measurement>(measurement);
            result.SpeedAngle = new Dictionary<int,double>();

            //filter selected
            int i = 0;
            foreach( var el in model.Measurement.SpeedAngle)
            {
                if(model.Selected[i])
                {
                    result.SpeedAngle.Add(el.Key,el.Value);
                }
                i++;
            }

            int mesId = _mservice.AddMeasurement(result);

            return RedirectToAction("index", new { Id = mesId} );
        }
    }
}