using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RheoSolutionClient.Data;
using RheoSolutionClient.Models;
using RheoSolutionClient.ViewModels;

using RheoSolutionClient.Services;
using AutoMapper;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace RheoSolutionClient.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        private ProjectService _pservice;
        private IMapper _mapper;
        public ProjectController(ProjectService pservice, IMapper mapper)
        {
            _pservice = pservice;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            return View(_pservice.GetAllProjects());
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public  IActionResult Create(ProjectViewModel input)
        {
            if(ModelState.IsValid)
            {
                var result = _mapper.Map<Project>(input);

                _pservice.AddProject(result);
            }

            return RedirectToAction("Index");
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            Project toMap = _pservice.GetProjectById(id);
            ProjectViewModel project = _mapper.Map<ProjectViewModel>(toMap);
            return View(project);
        }

        [HttpPost]
        public  IActionResult Edit(ProjectViewModel input)
        {
            if(ModelState.IsValid)
            {
                var result = _mapper.Map<Project>(input);
                _pservice.EditProject(result);
            }

            return RedirectToAction("Index");
        }


        [HttpGet]
        public  IActionResult Delete(int id)
        {
            _pservice.DeleteProject(id);

            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Details(int id)
        {
            Project selected = _pservice.GetProjectById(id);
            selected.Measurements = _pservice.GetMeasurementsByProjectId(id);
            return View(_mapper.Map<ProjectViewModel>(selected));
        }

    }
}