﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RheoSolutionClient.ApiModels;
using RheoSolutionClient.Data;
using RheoSolutionClient.Models;
using RheoSolutionClient.Services;
using RheoSolutionClient.ViewModels;

namespace RheoSolutionClient.Controllers
{
    public class HomeController : Controller
    {
        private RheoSolutionApiService _apiclient;
        public HomeController( RheoSolutionApiService apiclient)
        {
            _apiclient = apiclient;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Calculate()
        {
            ViewData["Message"] = "Calculation page.";

            return View();
        }

        [HttpPost]
        public IActionResult Calculate(RequestModel request)
        {
            int tempC = request.Selected.Count(s => s);
            double[] tempA = new double[tempC];
            int[] tempR = new int[tempC];
            int j = 0;
            for(int i = 0; i <12;i++)
            {
                if(request.Selected[i])
                {
                    tempA[j] = request.Angle[i];
                    tempR[j] = request.Rate[i];
                    j++;
                }
            }

            request.Angle = tempA;
            request.Rate = tempR;

            RheologyResponseModel rheology =  _apiclient.GetRheologyAsync(request).Result;
            List<RheoModelResponseModel> models = _apiclient.GetAllRheoModelsAsync(request).Result;

            RheoTotalViewModel output = new RheoTotalViewModel(){
                Models = models,
                Rheology = rheology,
                Bob = request.Bob,
                Rotor = request.Rotor,
                Spring = request.Spring,
            };


            ViewData["Message"] = "Calculation page.";

            return View("Calculated", output);
        }
        public IActionResult TechnologyStack()
        {
            ViewData["Message"] = "Technologies used in the project";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
