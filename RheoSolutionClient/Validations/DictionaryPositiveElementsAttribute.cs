using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RheoSolutionClient.Validations
{
    public class DictionaryPositiveElementsAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var dict = value as Dictionary<int,double>;

            bool valid = true;
            foreach (var el in dict)
            {
                if (el.Value<=0)
                {
                    valid = false;
                }
            }



            return valid
                ? ValidationResult.Success
                : new ValidationResult($"{validationContext.DisplayName} requires only positive elements");
        }
    }
}