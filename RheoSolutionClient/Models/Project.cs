using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RheoSolutionClient.Models
{

    public class Project
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(250)]
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string OwnerId { get; set; }
        //relations
        public List<Measurement> Measurements { get; set; }

    }
}