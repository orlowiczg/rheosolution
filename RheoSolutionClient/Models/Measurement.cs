using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Newtonsoft.Json;
using RheoSolutionClient.Globals;

namespace RheoSolutionClient.Models
{
    public class Measurement
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(250)]
        public string Description { get; set; }

        [Required]
        public BobsEnum Bob {get; set;}
        [Required]
        public RotorsEnum Rotor {get; set;}
        [Required]
        public SpringsEnum Spring {get; set;}

        [Required]
        [NotMapped]
        public Dictionary<int,double> SpeedAngle { get; set; } = new Dictionary<int, double>();

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        //relations
        [ForeignKey("ProjectId")]
        public int ProjectId { get; set; }
        public Project Project { get; set; }




        //serializing SpeedAngle
        [Obsolete("Only for Persistence by EntityFramework")]
        public string SpeedAngleJsonForDb
        {
            get
            {
                return SpeedAngle == null || !SpeedAngle.Any()
                        ? null
                        : JsonConvert.SerializeObject(SpeedAngle);
            }

            set
            {
                if (string.IsNullOrWhiteSpace(value))
                SpeedAngle.Clear();
                else
                SpeedAngle = JsonConvert.DeserializeObject<Dictionary<int, double>>(value);
            }
        }
            }
}