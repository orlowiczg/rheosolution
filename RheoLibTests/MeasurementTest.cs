using NUnit.Framework;
using RheoLib;
using RheoLib.RheoModels;

namespace Tests
{
    [TestFixture]
    public class MeasurementTest
    {
        [Test]
        public void Measurement1()
        {
            double[] teta = {2, 2, 3, 4, 6, 10, 14, 28, 48, 91, 141, 237};
            int[] rate = {1,2,3,6,10,20,30,60,100,200,300,600};
            string bob = "B1";
            string rotor = "R1";
            string spring = "F1";

            Measurement mes1 = new Measurement(teta, rate,
            FannParameters.CyllinderRadius[rotor],
            FannParameters.CyllinderRadius[bob],
            FannParameters.SpringType[spring],
            FannParameters.ShearStress[bob]);

        }
        [Test]
        public void Measurement2()
        {
            double[] teta = {0,0,0,0,0,0,0,0,0,0,0,0};
            int[] rate = {1,2,3,6,10,20,30,60,100,200,300,600};

            string bob = "B1";
            string rotor = "R1";
            string spring = "F1";

            Measurement mes1 = new Measurement(teta, rate,
            FannParameters.CyllinderRadius[rotor],
            FannParameters.CyllinderRadius[bob],
            FannParameters.SpringType[spring],
            FannParameters.ShearStress[bob]);

        }



    }
}