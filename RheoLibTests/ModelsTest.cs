using NUnit.Framework;
using RheoLib;
using RheoLib.RheoModels;
using System;

namespace Tests
{
    [TestFixture]
    public class ModelsTestForMeasurement1
    {

        int[] rate = {1, 2, 3, 6, 10, 20 ,30, 60, 100 ,200 ,300, 600};
        double[] teta = {2, 2, 3, 4, 6, 10, 14, 28, 48, 91, 141, 237};
        string bob = "B1";
        string rotor = "R1";
        string spring = "F1";
        Measurement mes1;

        [SetUp]
        public void SetUp()
        {
            mes1 = new Measurement(teta, rate,
            FannParameters.CyllinderRadius[rotor],
            FannParameters.CyllinderRadius[bob],
            FannParameters.SpringType[spring],
            FannParameters.ShearStress[bob]);
        }

        [Test]
        public void NewtonModelCalc()
        {
            double actualR = 0.9953;
            double actualF = 1066.00;
            double actualU = 143.59;
            double actualDV = 0.1247;

            IRheoModel newton = new NewtonModel(mes1);
            Assert.AreEqual(actualDV,newton.SpecificParams["Dynamic Viscosity"],0.001d);
            Assert.AreEqual(actualR,newton.R,0.001d);
            Assert.AreEqual(actualU,newton.U,0.01d);
            Assert.AreEqual(actualF,newton.F,0.01d);
        }

        [Test]
        public void BinghamModelCalc()
        {
            double actualR = 0.9964;
            double actualF = 1378.78;
            double actualU = 111.25;
            double actualPV = 0.1217;
            double actualYP = 1.9520;

            IRheoModel bingham = new BinghamModel(mes1);
            Assert.AreEqual(actualPV,bingham.SpecificParams["Plastic Viscosity"],0.001d);
            Assert.AreEqual(actualYP,bingham.SpecificParams["Yield Point"],0.001d);
            Assert.AreEqual(actualR,bingham.R,0.001d);
            Assert.AreEqual(actualU,bingham.U,0.01d);
            Assert.AreEqual(actualF,bingham.F,0.01d);
        }
        [Test]
        public void OdWModelCalc()
        {
            double actualR = 0.9801;
            double actualF = 243.85;
            double actualU = 608.63;
            double actualBFI = 0.8013;
            double actualCI = 0.3933;

            IRheoModel odw = new OstwaldDeWaeleModel(mes1);
            Assert.AreEqual(actualBFI,odw.SpecificParams["Behavior Flow Index"],0.001d);
            Assert.AreEqual(actualCI,odw.SpecificParams["Consistency Index"],0.001d);
            Assert.AreEqual(actualR,odw.R,0.001d);
            Assert.AreEqual(actualU,odw.U,0.01d);
            Assert.AreEqual(actualF,odw.F,0.01d);
        }
        [Test]
        public void CassonModelCalc()
        {
            double actualR = 0.9972;
            double actualF = 1750.87;
            double actualU = 87.74;
            double actualCV = 0.1160;
            double actualYP = 0.1669;

            IRheoModel casson = new CassonModel(mes1);
            Assert.AreEqual(actualCV,casson.SpecificParams["Casson Viscosity"],0.005d);
            Assert.AreEqual(actualYP,casson.SpecificParams["Yield Point"],0.005d);
            Assert.AreEqual(actualR,casson.R,0.001d);
            Assert.AreEqual(actualU,casson.U,0.01d);
            Assert.AreEqual(actualF,casson.F,0.01d);
        }

        [Test]
        public void HBModelCalc()
        {
            // double actualR = 0.9988;
            // double actualF = 4104.16;
            // double actualU = 37.55;
            // double actualYP = -0.6738;
            // double actualCI = 0.3184;
            // double actualBFI = 0.8601;


        //     IRheoModel casson = new CassonModel("Mes1", mes1);
        //     Assert.AreEqual(actualCV,casson.SpecificParams["Casson Viscosity"],0.005d);
        //     Assert.AreEqual(actualYP,casson.SpecificParams["Yield Point"],0.005d);
        //     Assert.AreEqual(actualR,casson.R,0.001d);
        //     Assert.AreEqual(actualU,casson.U,0.01d);
        //     Assert.AreEqual(actualF,casson.F,0.01d);
        //
        }
    }


    [TestFixture]
    public class ModelsTestForMeasurement2
    {

        double[] teta = {23, 24, 26, 28, 31, 35, 38, 45, 51, 65, 76, 98};
        int[] rate = {1,2,3,6,10,20,30,60,100,200,300,600};
        string bob = "B1";
        string rotor = "R1";
        string spring = "F1";
        Measurement mes1;

        [SetUp]
        public void SetUp()
        {
            mes1 = new Measurement(teta, rate,
            FannParameters.CyllinderRadius[rotor],
            FannParameters.CyllinderRadius[bob],
            FannParameters.SpringType[spring],
            FannParameters.ShearStress[bob]);
        }

        [Test]
        public void NewtonModelCalc()
        {
            double actualR = 0.0000;
            double actualF = 0.00;
            double actualU = 2259.15;
            double actualDV = 0.0623;

            IRheoModel newton = new NewtonModel(mes1);
            Assert.AreEqual(actualDV,newton.SpecificParams["Dynamic Viscosity"],0.001d);
            Assert.AreEqual(actualR,newton.R,0.001d);
            Assert.AreEqual(actualU,newton.U,0.01d);
            Assert.AreEqual(actualF,newton.F,0.01d);
        }

        [Test]
        public void BinghamModelCalc()
        {
            double actualR = 0.9604;
            double actualF = 118.90;
            double actualU = 124.10;
            double actualPV = 0.0377;
            double actualYP = 15.8608;

            IRheoModel bingham = new BinghamModel(mes1);
            Assert.AreEqual(actualPV,bingham.SpecificParams["Plastic Viscosity"],0.005d);
            Assert.AreEqual(actualYP,bingham.SpecificParams["Yield Point"],0.005d);
            Assert.AreEqual(actualR,bingham.R,0.001d);
            Assert.AreEqual(actualU,bingham.U,0.01d);
            Assert.AreEqual(actualF,bingham.F,0.01d);
        }
        [Test]
        public void OdWModelCalc()
        {
            double actualR = 0.9709;
            double actualF = 164.18;
            double actualU = 91.84;
            double actualBFI = 0.2241;
            double actualCI = 8.9254;

            IRheoModel odw = new OstwaldDeWaeleModel(mes1);
            Assert.AreEqual(actualBFI,odw.SpecificParams["Behavior Flow Index"],0.005d);
            Assert.AreEqual(actualCI,odw.SpecificParams["Consistency Index"],0.005d);
            Assert.AreEqual(actualR,odw.R,0.001d);
            Assert.AreEqual(actualU,odw.U,0.01d);
            Assert.AreEqual(actualF,odw.F,0.01d);
        }

        [Test]
        public void CassonModelCalc()
        {
            double actualR = 0.9942;
            double actualF = 849.82;
            double actualU = 18.60;
            double actualCV = 0.0145;
            double actualYP = 11.8235;

            IRheoModel casson = new CassonModel(mes1);
            Assert.AreEqual(actualCV,casson.SpecificParams["Casson Viscosity"],0.005d);
            Assert.AreEqual(actualYP,casson.SpecificParams["Yield Point"],0.005d);
            Assert.AreEqual(actualR,casson.R,0.001d);
            Assert.AreEqual(actualU,casson.U,0.01d);
            Assert.AreEqual(actualF,casson.F,0.01d);
        }

        [Test]
        public void HBModelCalc()
        {
            // double actualR = 0.9997;
            // double actualF = 18830.98;
            // double actualU = 0.85;
            // double actualYP = 10.2426;
            // double actualCI = 1.2857;
            // double actualBFI = 0.4956;


        //     IRheoModel casson = new CassonModel("Mes1", mes1);
        //     Assert.AreEqual(actualCV,casson.SpecificParams["Casson Viscosity"],0.005d);
        //     Assert.AreEqual(actualYP,casson.SpecificParams["Yield Point"],0.005d);
        //     Assert.AreEqual(actualR,casson.R,0.001d);
        //     Assert.AreEqual(actualU,casson.U,0.01d);
        //     Assert.AreEqual(actualF,casson.F,0.01d);
        //
        }
    }
}