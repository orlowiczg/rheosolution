using NUnit.Framework;
using RheoLib.Utils;
using System;
namespace Tests
{
    [TestFixture]
    public class MatlabArrayTest
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void ConstructorWrongInput()
        {
            MatlabArray matlabarray = null;
            double[] doublearray = null;
            MatlabArray arr1;

            Assert.Throws<ArgumentException>( () =>  arr1 = new MatlabArray(0));
            Assert.Throws<ArgumentException>( () =>  arr1 = new MatlabArray(new double[0]));
            Assert.Throws<NullReferenceException>( () =>  arr1 = new MatlabArray(doublearray));
            Assert.Throws<NullReferenceException>( () =>  arr1 = new MatlabArray(matlabarray));
        }
        [Test]
        public void NotInitialized()
        {
            MatlabArray arr1 = null;
            MatlabArray arr2 = null;
            MatlabArray arr3;
            Assert.Throws<NullReferenceException>( () =>  arr3 = arr1+arr2);
            Assert.Throws<NullReferenceException>( () =>  arr3 = arr1+1);
            Assert.Throws<NullReferenceException>( () =>  arr3 = 1+arr2);

            Assert.Throws<NullReferenceException>( () =>  arr3 = arr1-arr2);
            Assert.Throws<NullReferenceException>( () =>  arr3 = arr1-1);
            Assert.Throws<NullReferenceException>( () =>  arr3 = 1-arr2);

            Assert.Throws<NullReferenceException>( () =>  arr3 = arr1*arr2);
            Assert.Throws<NullReferenceException>( () =>  arr3 = arr1*1);
            Assert.Throws<NullReferenceException>( () =>  arr3 = 1*arr2);

            Assert.Throws<NullReferenceException>( () =>  arr3 = arr1/arr2);
            Assert.Throws<NullReferenceException>( () =>  arr3 = arr1/1);
            Assert.Throws<NullReferenceException>( () =>  arr3 = 1/arr2);

            Assert.Throws<NullReferenceException>( () =>  arr3 = MatlabArray.Logn(arr1));
            Assert.Throws<NullReferenceException>( () =>  arr3 = arr1^1);
            Assert.Throws<NullReferenceException>( () =>  MatlabArray.Sum(arr1));
            Assert.Throws<NullReferenceException>( () =>  MatlabArray.Mean(arr1));

        }

        [Test]
        public void ZeroDivision()
        {
            MatlabArray arr1 = new MatlabArray(10);
            MatlabArray arr2 = new MatlabArray(10);
            MatlabArray arr3;

            arr1.FillRandom(1,10);
            arr2.FillRandom(1,10);
            arr2[2] = 0;

            Assert.Throws<DivideByZeroException>( () =>  arr3 = arr1/0);
            Assert.Throws<DivideByZeroException>( () =>  arr3 = arr1/arr2);


        }



        [Test]
        public void CorrectSumming()
        {
            MatlabArray arr = new MatlabArray(new double[5]{1,2,3,4,5});
            Assert.AreEqual(15,MatlabArray.Sum(arr));

            arr = new MatlabArray(new double[5]{1,4,3,5,5});
            Assert.AreEqual(18,MatlabArray.Sum(arr));

            arr = new MatlabArray(new double[3]{5,100,-3});
            Assert.AreEqual(102,MatlabArray.Sum(arr));
        }

        [Test]
        public void CorrectMeanValue()
        {
            MatlabArray arr = new MatlabArray(new double[5]{1,2,3,4,5});
            Assert.AreEqual(3.0,MatlabArray.Mean(arr));

            arr = new MatlabArray(new double[5]{1,4,3,5,5});
            Assert.AreEqual(18.0/5.0,MatlabArray.Mean(arr),0.001);

           arr = new MatlabArray(new double[3]{5,100,-3});
            Assert.AreEqual(34.0,MatlabArray.Mean(arr));
        }

        [Test]
        public void PowerOperator()
        {
            MatlabArray arr = new MatlabArray(5);
            arr.Fill(2);
            MatlabArray arrpowered = arr^3;
            Assert.AreEqual(8,arrpowered[0]);
            Assert.AreEqual(8,arrpowered[1]);
            Assert.AreEqual(8,arrpowered[2]);
            Assert.AreEqual(8,arrpowered[3]);
            Assert.AreEqual(8,arrpowered[4]);

            arr = new MatlabArray(new double[4]{1,2,4,6});
            arrpowered = arr^2;
            Assert.AreEqual(1,arrpowered[0]);
            Assert.AreEqual(4,arrpowered[1]);
            Assert.AreEqual(16,arrpowered[2]);
            Assert.AreEqual(36,arrpowered[3]);
        }
    }
}